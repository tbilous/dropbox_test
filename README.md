# README

### Rules
Requirements
- REST API
- Web Application
- Ruby on Rails based API
- Popular browsers support
- Dropbox API (desirable)

Description:
Web Application of basic file explorer should be provided as a result of this assignment.
The UI part should consist of the toolbar and content area. There can be any number of buttons on
the toolbar, any non‐functional buttons should be “stubs” and give users a “Not implemented”
message when clicked.
The content area is a list with 4 columns:
1) checkbox to select a row;
2) file/folder name;
3) file size;
4) file/folder modification date
Each row should have “on hover” effect and be highlighted when the mouse pointer enters the row.
A small arrow should appear under the row indicating a context menu is available.
The context menu appears when the mouse left button is pressed and should allow removing this
particular row.
It is required to implement the navigation part of the file explorer, i.e. open files and sub-folders.
It is required to read data from a remote source via REST service.
Ideally a document list from a public Dropbox folder (via Server app) would be considered a first class
application.
Please fully document your solution and ensure that it will build easily on the reviewers PC.
– if any extra steps to compile are required then please list these in a separate Word document if required.


* Ruby version 2.5.0, rails version 5.2
### Setup
* Rename database.yml.example, secrets.yml.example
* Write your dropbox token to ENV DROPBOX_KEY
```
bundle install
yarn install
rake db:create
rake db:migrate
```
* Start dev server `foreman start -f Procfile.dev`, address in browser http://lvh.me:3000
* Test `rspec spec/`
