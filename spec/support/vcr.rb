VCR.configure do |c|
  c.allow_http_connections_when_no_cassette = true
  c.ignore_localhost = true
  c.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  c.hook_into :webmock
  c.default_cassette_options = { record: :once }
  c.default_cassette_options = {
      :match_requests_on => [:method,
                             VCR.request_matchers.uri_without_param(:timestamp)]
  }
  c.filter_sensitive_data("<TOKEN>") { ENV['DROPBOX_KEY'] }
end

RSpec.configure do |config|
  # Add VCR to all tests
  config.around(:each) do |example|
    vcr_tag = example.metadata[:vcr]
    vcr_off_tag = example.metadata[:vcr_off]

    if vcr_off_tag
      VCR.turned_off(&example)
    else
      options = vcr_tag.is_a?(Hash) ? vcr_tag : {}
      path_data = [example.metadata[:description]]
      parent = example.example_group
      while parent != RSpec::ExampleGroups
        path_data << parent.metadata[:description]
        parent = parent.parent
      end

      name = path_data.map{|str| str.underscore.gsub(/\./,'').gsub(/[^\w\/]+/, '_').gsub(/\/$/, '')}.reverse.join("/")

      VCR.use_cassette(name, options, &example)
    end
  end
end
