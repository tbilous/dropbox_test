require 'webmock/rspec'

RSpec.configure do |config|
  WebMock.disable_net_connect!(allow_localhost: true)
  config.before(:each, :vcr_off) do
    WebMock.disable!
  end
  config.after(:each, :vcr_off) do
    WebMock.disable!
    WebMock.disable_net_connect!(allow_localhost: true)
  end
end
