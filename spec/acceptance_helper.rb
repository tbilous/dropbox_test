require 'rails_helper'
require 'puma'
require 'i18n'
require 'capybara/webkit'

RSpec.configure do |config|
  include ActionView::RecordIdentifier

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end

  Capybara.default_max_wait_time = 2
  Capybara.save_path = './tmp/capybara_output'
  Capybara.always_include_port = true # for correct app_host

  Capybara.javascript_driver = :webkit

  Capybara::Webkit.configure do |webkit|
    webkit.allow_url('10.0.2.15')
    webkit.allow_url('fonts.googleapis.com')
    webkit.allow_url('fonts.gstatic.com')
    webkit.allow_url('maxcdn.bootstrapcdn.com')
  end

  Capybara.server = :puma

  config.use_transactional_fixtures = false

  config.before(:suite) { DatabaseCleaner.clean_with :truncation }

  config.before(:each) { DatabaseCleaner.strategy = :transaction }

  config.before(:each, js: true) { DatabaseCleaner.strategy = :truncation }

  config.before(:each) { DatabaseCleaner.start }

  config.append_after(:each) do
    DatabaseCleaner.clean
  end
end
