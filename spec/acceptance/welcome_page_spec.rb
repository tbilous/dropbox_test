require 'acceptance_helper'

feature 'Droppbox file manager', %q{
  In order to list dropbox files and folders
} do

  context 'Droppbox file manager', :js do
    it 'user can to see files list' do
      visit root_path
      expect(page).to have_content 'folder 1'
      expect(page).to have_content 'folder 2'
      expect(page).to have_content 'How to use the Public folder.txt'
    end

    it 'user can to proceed folders' do
      visit root_path
      click_link 'folder 1'
      expect(page).to_not have_content 'folder 1'
      expect(page).to_not have_content 'folder 2'
      expect(page).to have_content 'How to use the Public folder.txt'
    end

    it 'user can to proceed empty folders' do
      visit root_path
      click_link 'folder 2'
      expect(page).to_not have_content 'folder 1'
      expect(page).to_not have_content 'folder 2'
      expect(page).to_not have_content 'How to use the Public folder.txt'
    end

    it 'user get toast message' do
      visit root_path
      first(:link, t('action')).click
      expect(page).to have_content t('n_in')
    end
  end
end
