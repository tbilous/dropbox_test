require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'GET #welcome' do
    before { get :welcome }
    it 'renders the welcome template' do
      expect(response).to render_template :welcome
    end
  end

  describe 'GET #reload' do
    let(:params) { { folder: '/Public' } }
    before { get :reload, params: params, format: :json }
    it 'returns Public' do
      expect(response).to match_json_schema('public')
    end
  end
end
