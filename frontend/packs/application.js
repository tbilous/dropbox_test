import "components/home/home";
import I18n from "i18n-js";
import Rails from "rails-ujs";
import "bootstrap-sass/assets/javascripts/bootstrap";
import "./css/app.scss";
import "./application.css";
import "./utils.coffee";
import translations from "./translations";

I18n.locale = document.documentElement.lang; // or window.locale
I18n.translations = translations;

Rails.start();

// eslint-disable-next-line no-console
console.log("Webpacker works!");
