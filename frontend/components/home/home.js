const $ = require("jquery");
const template = require("./content_wrapper.hbs");

const container = document.querySelector("#files");

const I18n = require("i18n-js");

const getQuery = query => {
  $.ajax({
    type: "GET",
    url: `/reload?folder=${query}`,
    success(data) {
      container.innerHTML = template(data);
    }
  });
};

if (container) {
  getQuery("/public");
}

$(document).on("ajax:success", ".js-reload", data => {
  container.innerHTML = template(data.detail[0]);
});

$(document).on("click", ".js-dingle", () => {
  window.App.utils.successMessage(I18n.t("n_in"));
});

$(document).on("mouseover", ".row-master", event => {
  const el = document.getElementById(event.currentTarget.dataset.target);
  el.classList.remove("hidden");
});

$(document).on("click", ".js-delete", event => {
  document
    .getElementById(event.currentTarget.dataset.target)
    .classList.add("hidden");
  document
    .getElementById(event.currentTarget.dataset.action)
    .classList.add("hidden");
});
