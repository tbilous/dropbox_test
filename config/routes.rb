Rails.application.routes.draw do
  get :reload, to: 'home#reload', defaults: { format: :json }
  get :welcome, to: 'home#welcome'

  root 'home#welcome'
end
