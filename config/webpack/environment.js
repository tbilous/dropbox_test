const { environment } = require("@rails/webpacker");
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require("webpack");

const coffee = require("./loaders/coffee");
const handlebars = require("./loaders/handlebars");
const railsViewLoader = require("./loaders/rails-view-loader.js");

environment.loaders.append("coffee", coffee);
environment.loaders.append("handlebars", handlebars);
environment.loaders.append("railsViewLoader", railsViewLoader);

environment.loaders.append("json", {
  test: /\.json$/,
  use: "json-loader"
});
environment.loaders.append("yaml", {
  test: /\.ya?ml$/,
  loaders: ["json-loader", "yaml-loader"]
});

environment.plugins.prepend(
  "Provide",
  new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
    jquery: "jquery",
    "window.Tether": "tether"
  })
);

const config = environment.toWebpackConfig();

config.resolve.alias = {
  jquery: "jquery/src/jquery"
};

module.exports = environment;
