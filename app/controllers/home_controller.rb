class HomeController < ApplicationController
  before_action :folder, :load_files

  def welcome; end

  def reload
    render json: { data: @files }
  end

  private

  def folder
    @folder = params[:folder].nil? ? '/public' : params[:folder]
  end

  def load_files
    @files = DropboxClient.new(@folder).list_folder
  end
end
