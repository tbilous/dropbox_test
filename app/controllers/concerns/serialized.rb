module Serialized
  extend ActiveSupport::Concern

  def render_json(item, root_name = controller_name.singularize)
    ApplicationController.render file: "components/#{controller_name}/_#{root_name}",
                                 locals: { "#{root_name}": item }, layout: false
    # if item.errors.any?
    #   render_errors(item)
    # else
    #   render_html = ApplicationController.render file: "components/#{controller_name}/_#{root_name}",
    #                                              locals: { "#{root_name}": item }, layout: false
    #   render_json_message
    # end
  end

  def render_json_message
    render json: { message: t('.message') }
  end

  def render_errors(object)
    render plain: object.errors.full_messages.join('<br/>'), status: :forbidden
  end

  def render_not_found
    render status: :not_found
  end
end
