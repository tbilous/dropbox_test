require 'application_responder'

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception
  # protect_from_forgery prepend: true
  prepend_view_path Rails.root.join('frontend')
end
