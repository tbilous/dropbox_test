module ApplicationHelper
  def render_flash
    javascript_tag('App.flash = JSON.parse(' "'#{j({ success: flash.notice, error: flash.alert }.to_json)}'" ');')
  end

  def app_name
    Rails.application.class.parent_name.underscore.humanize.split.map(&:capitalize).join(' ')
  end

  def full_title(page_title)
    base_title = app_name
    if page_title.empty?
      base_title
    else
      "#{base_title} #{page_title}"
    end
  end

  def component(component_name, locals = {}, &block)
    folder = component_name.split('_').first
    name = component_name.split('_').last
    render("components/#{folder}/#{name}", locals, &block)
  end

  alias c component
end
