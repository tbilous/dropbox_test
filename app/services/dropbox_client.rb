class DropboxClient
  include ActionView::Helpers

  def initialize(options)
    @client = DropboxApi::Client.new(ENV['DROPBOX_KEY'])
    @options = options || ''
  end

  def list_folder
    data = @client.list_folder(@options, include_media_info: true).entries.map(&:to_hash)
    { files: humanize_files(data), folders: humanize_folders(data), level: level, previous: previous }
  end

  def metadata
    @client.get_metadata(@options).to_hash
  end

  private

  Folders = Struct.new(:id,
                       :name,
                       :path_display)

  Files = Struct.new(:id,
                     :name,
                     :path_display,
                     :client_modified,
                     :size)

  def humanize_folders(data)
    data.map do |i|
      next if i['.tag'] == 'file'
      Folders.new i['id'], i['name'], i['path_display']
    end.compact
  end

  def humanize_files(data)
    data.map do |i|
      next if i['.tag'] == 'folder'
      Files.new i['id'], i['name'], i['path_display'],
                i['client_modified'].to_time.to_formatted_s(:short),
                number_to_human_size(i['size'])
    end.compact
  end

  def level
    @options.empty? ? 1 : @options.split('/').count - 1
  end

  def previous
    arr = @options.split('/')
    arr.pop(1)
    arr.join('/')
  end
end
